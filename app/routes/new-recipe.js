import Route from '@ember/routing/route';
import { queryManager } from "ember-apollo-client";
import query from "recipes-app/gql/queries/allCategories";

export default Route.extend({
    apollo: queryManager(),

    model(params) {
        let variables = {};
        return this.apollo.watchQuery({ query, variables }, "allCategories").catch(error => alert(error));
    }
});
