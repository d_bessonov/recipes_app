import Route from '@ember/routing/route';
import { queryManager } from "ember-apollo-client";
import query from "recipes-app/gql/queries/recipe";
// import gql from "graphql-tag";

export default Route.extend({
  apollo: queryManager(),

  model(params) {
    // const query = gql`
    // query allPosts {
    //     allPosts {
    //         title
    //         body
    //     }
    // }
    // `;

    // const query = gql`
    // query post($id: ID!) {
    //     post(id: $id) {
    //         title
    //         body
    //     }
    // }
    // `;
    
    let variables = { id: params.recipe_id };
    
    return this.apollo.watchQuery({ query, variables }, "recipe");
  }
});
