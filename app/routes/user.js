import Route from '@ember/routing/route';
import { queryManager } from "ember-apollo-client";
import query from "recipes-app/gql/queries/user";

export default Route.extend({
    apollo: queryManager(),

    model(params) {
        let variables = { id: params.user_id };
        return this.apollo.watchQuery({ query, variables }, "user").catch(error => alert(error));
    }
});
