import Route from '@ember/routing/route';
import { queryManager } from "ember-apollo-client";
import query from "recipes-app/gql/queries/allRecipes";


export default Route.extend({
    apollo: queryManager(),
    // apollo: Ember.inject.service(),

    model(params) {
        let variables = {};
        return this.apollo.watchQuery({ query, variables }, "allRecipes").catch(error => alert(error));
        // return this.get('apollo').query({query}, "allPosts").then(response => console.log(response));

        // return [
        //     {
        //         id: '1',
        //         title: 'First',
        //         body: 'First post'
        //     },
        //     {
        //         id: '2',
        //         title: 'Second',
        //         body: 'Second post'
        //     }
        // ]
    }
});
