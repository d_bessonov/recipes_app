import Route from '@ember/routing/route';
import { queryManager } from "ember-apollo-client";
import query from "recipes-app/gql/queries/category";

export default Route.extend({
    apollo: queryManager(),

    model(params) {

        let variables = { id: params.category_id };
        return this.apollo.watchQuery({ query, variables }, "category").catch(error => alert(error));
        // return [
        //     {
        //       id: params.id,
        //       title: 'Params title',
        //       body: 'Params body'
        //     }
        //   ]
    }
});
