import Controller from '@ember/controller';
import { inject as service } from "@ember/service";
import mutation from "recipes-app/gql/mutations/createRecipe";

export default Controller.extend({
  apollo: service(),
  selectedCategories: Ember.A(),

  image: '',
  
  actions: {
    submit() {
      console.log(this.get("model"));
      let variables = {
        name: this.get("name"),
        time: this.get("time"),
        ingredients: this.get("ingredients"),
        directions: this.get("directions"),
        categoriesId: this.get("selectedCategories").map(category => { return category.id }),
        image: this.image
      };
      return this.get("apollo").mutate({ mutation, variables }, "createRecipe")
        .then(() => { this.transitionToRoute('recipes'); })
        .catch((e) => {
          console.log(e);
          this.transitionToRoute('recipes');
        });
      
    },

    uploadImage() {
        console.log("upload");
    },

    didSelectFiles(listOfFiles) {
      const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });

      toBase64(listOfFiles[0]).then((resolve, reject) => { this.image = resolve });
    }
  }

});
