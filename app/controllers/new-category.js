import Controller from '@ember/controller';
import { inject as service } from "@ember/service";
import mutation from "recipes-app/gql/mutations/createCategory";


export default Controller.extend({
  apollo: service(),
  selectedCategories: Ember.A(),

  image: '',
  
  actions: {
    submit() {
      console.log(this.get("model"));
      let variables = {
        name: this.get("name"),
        description: this.get("description"),
        image: this.image
      };
      return this.get("apollo").mutate({ mutation, variables }, "createCategory")
        .then((resolve, reject) => {
          if (reject) console.log(reject);
          this.transitionToRoute('categories');
        })
        .catch((e) => {
          console.log(e);
          this.transitionToRoute('categories');
        });
    },

    didSelectFiles(listOfFiles) {
      const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });

      toBase64(listOfFiles[0]).then((resolve, reject) => { this.image = resolve });
    }
  }
});
