import Component from '@ember/component';

export default Component.extend({
  attributeBindings: ['multiple', 'type'],
  tagName: 'input',
  type: 'file',
  accept: "image/png,image/jpeg,application/pdf",
  multiple: true,
  files: null, 
});
