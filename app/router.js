import EmberRouter from '@ember/routing/router';
import config from './config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('recipe', { path: '/recipes/:recipe_id'});
  this.route('recipes');
  this.route('category', { path: '/categories/:category_id'});
  this.route('categories');
  this.route('user', { path: '/users/:user_id' });
  this.route('search');
  this.route('new-recipe');
  this.route('new-category');
});
