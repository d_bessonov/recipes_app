import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | new-category', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:new-category');
    assert.ok(route);
  });
});
